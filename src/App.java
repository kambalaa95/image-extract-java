import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class App {
    public static void main(String[] args) throws IOException {
        Path srcFile = Paths.get("C:\\Users\\Atha\\AppData\\Local\\Packages\\Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy\\LocalState\\Assets");
        Path destDir = Paths.get("C:\\Users\\Atha\\AppData\\Local\\Packages\\Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy\\LocalState\\Assets\\temp");
        Path bgPicsPath = Paths.get("C:\\Users\\Atha\\Pictures\\Saved Pictures");
        
        FileControl fileControl = new FileControl(srcFile, destDir, bgPicsPath);
        FileControl.copyFilesToTemp(srcFile, destDir); // copies files into temp folder
        fileControl.filterImages(destDir, bgPicsPath); // deletes all non-desktop resolution images 
                                           // and copies them into final windows Background pictures folder
    }   
}

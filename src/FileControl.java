import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class FileControl {
    private final Path srcPath;
    private final Path tempPath;
    private final Path bgPicsPath;
    
    FileControl(final Path srcPath, final Path tempPath, final Path bgPicsPath){
        this.srcPath = srcPath;
        this.tempPath = tempPath;
        this.bgPicsPath = bgPicsPath;
    }
    
    public static void copyFilesToTemp(final Path srcPath, final Path tempPath) throws IOException {
        int countCoppiedToTemp = 0;
        for(final Path path : Files.newDirectoryStream(srcPath)){
            String dirName = path.getFileName().toString();
            if(!dirName.equals("temp")){ // temp is a folder name within the source directory, dont wanna copy that
                try {
                    final Path completePath = tempPath.resolve(path.getFileName() + ".jpg");
                    Files.copy(path, completePath, StandardCopyOption.REPLACE_EXISTING); // change destination uri by adding file name at the end, C:MyFolder --> C:MyFolder/file.txt
                    countCoppiedToTemp++;
//                    System.out.println("Copied: " + tempPath.resolve(path.getFileName()));
                }catch (IOException ioExc){
                    System.err.format("Error when copying files into temp folder.");
                }
            }
        }
        System.out.println(countCoppiedToTemp + " files were coppied to temp folder.");
    }
    
    public static void filterImages(final Path tempPath, final Path bgPicsPath) throws IOException{
        int countDesktopImages = 0;
        for(final Path path : Files.newDirectoryStream(tempPath)){ 
            BufferedImage buffImg = ImageIO.read(new File(path.toString()));
            int buffImgWidth = buffImg.getWidth();
            if(buffImgWidth == 1920){ // if desktop resolution
                try{
                    final Path completePath = bgPicsPath.resolve(path.getFileName());
                    Files.copy(path, completePath, StandardCopyOption.REPLACE_EXISTING);
                    countDesktopImages++;
                }catch(IOException ioExc){
                    System.err.println("Error when copying images into Background Pictures folder.");
                }
            }else{
                Files.delete(path);
            }
        }
        System.out.println(countDesktopImages+ " images transferred to Background folder.");
    }
    
    // tester function to see closer how it works
    public static void copyOneFile(final Path srcPath, final Path tempPath){
        try{
            Files.copy(srcPath, tempPath, StandardCopyOption.REPLACE_EXISTING);
            System.out.println("File coppied successfully.");
        }catch(IOException ioExc){
            System.err.format("Error when copying file.");
        }
    }
    
}
